var net = require("net");
var JSONStream = require('JSONStream');

var Game = require('./game.js');
var myGame = new Game();

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {

  return send({
    msgType: "join",
    data: {
      name: botName,
      key: botKey
    }
  });
  
  // return send({
  //   msgType: 'createRace',
  //   data: {
  //     botId: {
  //       name: botName,
  //       key: botKey
  //     },
  //     "trackName": "usa",
  //     "password": "schumi4ever",
  //     "carCount": 1
  //   }
  // });
  
});

function send(json) {
  client.write(JSON.stringify(json));
  return client.write('\n');
}

jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function(data) {
  if (data.msgType === 'carPositions') {

    // send({
    //   msgType: "throttle",
    //   data: 0.5
    // });
    
    var switchResponse = myGame.getAISwitchDecision(data);
    if (switchResponse !== null) {
      console.log(switchResponse);
      send(switchResponse);
    }
    // the first 'carPositions' has no gameTick! the next one (after gameStart) has gameTick 1.
    send(myGame.getAISpeedDecision(data));
    // send({msgType: "throttle", data: 0.5});

  } else {
    if (data.msgType === 'yourCar') {

      myGame.setCar(data);

    } else if (data.msgType === 'gameInit') {

      myGame.loadData(data);

    } else if (data.msgType === 'turboAvailable') {

      console.log("TURBO AVAILABLE (for " + data['data'].turboDurationTicks+" ticks, factor: "+data['data'].turboFactor + ")");
      myGame.setTurbo(data);

    } else if (data.msgType === 'join') {
      console.log('Joined');
    } else if (data.msgType === 'gameStart') {
      console.log('Race started');
    } else if (data.msgType === 'gameEnd') {
      console.log('Race ended');
    } else if (data.msgType === 'tournamentEnd') {
      console.log('Tournament end');
    } else if (data.msgType === 'crash') {
      console.log(data.data.name+' ('+data.data.color+') '+'crashed at gameTick: ' + data.gameTick);
    } else if (data.msgType === 'spawn') {
      console.log(data.data.name+' ('+data.data.color+') '+'spawned at gameTick: ' + data.gameTick);
    } else if (data.msgType === 'lapFinished') {
      console.log('lap finished: '+data.data.car.name+' ('+data.data.car.color+') laptime: '+data.data.lapTime.ticks);
    } else if (data.msgType === 'dnf') { // did not finish
      console.log('dnf');
    } else if (data.msgType === 'finish') {
      console.log('finished');
    }
    send({
      msgType: "ping",
      data: {}
    });
  }
});

jsonStream.on('error', function() {
  return console.log("disconnected");
});
