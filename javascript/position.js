module.exports = Position;
var method = Position.prototype;

function Position(piecePositionJSON, track, previousPosition) {
	this.track = track;
	this.pieceIndex = piecePositionJSON['pieceIndex'];
	this.inPieceDistance = piecePositionJSON['inPieceDistance'];
	this.lap = piecePositionJSON['lap'];
	this.startLaneIndex = piecePositionJSON['lane']['startLaneIndex'];
	this.endLaneIndex = piecePositionJSON['lane']['endLaneIndex'];

	// TODO:
	// this.totalDistanceTravelled = this.calculateDistanceTravelled(track, this.lap);
	this.distanceTravelledSinceLastPosition = this.calculateDistanceTravelledSinceLastPosition(track, previousPosition);
}

method.getCurrentLap = function() {return this.lap;}
method.getPieceIndex = function() {return this.pieceIndex;};
method.getTrackPiece = function() {return this.track.getTrackPiece(this.pieceIndex);};
method.getInPieceDistance = function() {return this.inPieceDistance;};
method.getStartLaneIndex = function() {return this.startLaneIndex;};
method.getEndLaneIndex = function() {return this.endLaneIndex;};
method.getDistanceSinceLastPosition = function() {return this.distanceTravelledSinceLastPosition;};

method.calculateLaneLengthbetweenNext2Switches = function(laneIndex) {
	var offset = this.track.getLane(laneIndex).distanceFromCenter;
	var pi = this.pieceIndex;
	var distance = 0;
	for (var i=pi+1 ; i < (pi + this.track['trackPieces'].length); i++) {
		if (this.track.getTrackPiece(i)['switch']) {
			// start counting
			for (var j = i+1 ; j < (i + this.track['trackPieces'].length); j++) {
				var piece = this.track.getTrackPiece(j%this.track['trackPieces'].length);
				if (piece['switch']) {
					return distance;
				} else {
					if (piece.getAngle() === null) {
						distance += piece['length'];
					} else {
						distance += piece.calculateTurnDistance(offset);
					}
				}
			}
			return distance;
		}
	}
	return distance;
}

/**
 * TODO: controleren of klopt.
 */
method.nextStraightIsToFinish = function() {
	if (this.lap === this.track.getTotalLaps()-1) {
		var PIList = [];
		var inturn = false;
		for (i = this.track['trackPieces'].length - 1; i >=0; i--) {
			if (this.track.getTrackPiece(i).getAngle() === null && !inturn) {
				if (this.pieceIndex === i) {return true;}
			} else if (this.track.getTrackPiece(i).getAngle() === null && inturn) {
				return false;
			} else {
				inturn = true;
				if (this.pieceIndex === i) {return true;}
			}
		}
	}
	return false;
}

method.getRemainingDistanceInPiece = function() {
	var distance;
	var piece = this.track.getTrackPiece(this.pieceIndex);

	if (piece.getAngle() == null) { // straigth piece
		distance = piece.getAverageLength() - this.getInPieceDistance();
	} else { // turn piece
		var radiusStartLane;
		var radiusEndLane;

		if (piece.getAngle() < 0) {
			radiusStartLane = piece.getRadius() + this.track.getLane(this.getStartLaneIndex()).distanceFromCenter;
			radiusEndLane = piece.getRadius() + this.track.getLane(this.getEndLaneIndex()).distanceFromCenter;
		} else {
			radiusStartLane = piece.getRadius() - this.track.getLane(this.getStartLaneIndex()).distanceFromCenter;
			radiusEndLane = piece.getRadius() - this.track.getLane(this.getEndLaneIndex()).distanceFromCenter;
		}
		var offset = this.track.getLane(this.getEndLaneIndex()).distanceFromCenter;
		distance = piece.calculateTurnDistance(offset) - this.getInPieceDistance()*(radiusEndLane/radiusStartLane);
		
		// DEBUG LOG
		// console.log('distance: ' + distance);
	}
	
	return distance;
}

/**
 * TODO: distance is negative when calculating with 2 turns (for the new piece)
 */
method.calculateDistanceTravelledSinceLastPosition = function(track, previousPosition) {
	var distance;

	if (previousPosition == null) {
		return this.inPieceDistance;
	}

	// same piece
	if (this.pieceIndex == previousPosition.getPieceIndex()) {
		distance = this.inPieceDistance - previousPosition.getInPieceDistance();
	}
	// different pieces
	else {
		// 1.calculate remainder of last piece
		var pI = previousPosition.getPieceIndex();
		var previousPiece = track.getTrackPiece(pI);
		if (previousPiece.getAngle() == null) { // straigth piece
			distance = previousPiece.getAverageLength() - previousPosition.getInPieceDistance();
		} else { // turn piece
			var previousRadiusStartLane;
			var previousRadiusEndLane;

			if (previousPiece.getAngle() < 0) {
				previousRadiusStartLane = previousPiece.getRadius() + track.getLane(previousPosition.getStartLaneIndex()).distanceFromCenter;
				previousRadiusEndLane = previousPiece.getRadius() + track.getLane(previousPosition.getEndLaneIndex()).distanceFromCenter;
			} else {
				previousRadiusStartLane = previousPiece.getRadius() - track.getLane(previousPosition.getStartLaneIndex()).distanceFromCenter;
				previousRadiusEndLane = previousPiece.getRadius() - track.getLane(previousPosition.getEndLaneIndex()).distanceFromCenter;
			}
			var offset = track.getLane(previousPosition.getEndLaneIndex()).distanceFromCenter;
			distance = previousPiece.calculateTurnDistance(offset) - previousPosition.getInPieceDistance()*(previousRadiusEndLane/previousRadiusStartLane);
			
			// DEBUG LOG
			// console.log('distance: ' + previousPiece.calculateTurnDistance(offset) +' - '+ previousPosition.getInPieceDistance()*(previousRadiusEndLane/previousRadiusStartLane));
			// console.log('this.inPieceDistance: '+this.inPieceDistance)
			// console.log('si: '+previousPosition.getEndLaneIndex()+'\tei: '+previousPosition.getStartLaneIndex())
		}

		// 2.add new inPieceDistance of current piece
		distance += this.inPieceDistance;
	}
	return distance;
};



/*
	returns distance from this position to the other position
	will return a negative number if other position is behind current position
*/
method.getDistanceTo = function(otherPos) {
	var curPosIsAhead = this.aheadOf(otherPos);
	var leader = curPosIsAhead ? this : otherPos;
	var loser = curPosIsAhead ? otherPos : this;
	// leader and loser are on the same piece
	if (leader.lap == loser.lap && leader.pieceIndex == loser.pieceIndex) {
		return (leader.inPieceDistance - loser.inPieceDistance) * (curPosIsAhead ? 1 : -1);
	}
	// leader and loser are on a different piece on the same lap
	else if (leader.lap == loser.lap) {
		var distance = leader.inPieceDistance;
		for (var i = leader.pieceIndex - 1; i > loser.pieceIndex; i--) {
			distance += game.pieces[i].length; // TODO: make global game variable
		}
		distance += game.pieces[loser.pieceIndex].length - loser.inPieceDistance;
		return distance  * (curPosIsAhead ? 1 : -1);
	}
	// leader and loser are on different laps
	else {
		// go to 1 lap difference
		var distance = game.trackLength * (leader.lap - loser.lap - 1);
		// distance from leader to begin of lap
		distance += leader.inPieceDistance;
		for (var i = leader.pieceIndex - 1; i >= 0; i--) {
			distance += game.pieces[i].length;
		}
		// distance from end of lap to loser
		for (var i = game.pieces.length - 1; i > loser.pieceIndex; i--) {
			distance += game.pieces[i].length;
		}
		distance += game.pieces[loser.pieceIndex].length - loser.inPieceDistance;
		return distance;
	}
};

/*
	returns true if we this position is ahead of argument, false otherwise
*/
method.aheadOf = function(otherPos) {
	if (otherPos.lap > this.lap) {
		return false;
	}
	else if (this.lap > otherPos) {
		return true;
	}
	else {
		if (otherPos.pieceIndex > this.pieceIndex) {
			return false;
		}
		else if (this.pieceIndex > otherPos.pieceIndex) {
			return true;
		}
		else {
			return (this.inPieceDistance > otherPos.inPieceDistance);
		}
	}
};

// TODO: take lanes into consideration
method.getDistanceToNextCorner = function() {
	if (this.getTrackPiece()['angle'] !== null) { // return 0 if we're in a corner
		return 0;
	}
	var distance = this.getRemainingDistanceInPiece(); // start with current piece
	var numNextPiecesToCheck = 10;
	var nextOnes = this.track.getNextXPieces(this.getPieceIndex(),numNextPiecesToCheck);
	for (var i = 0; i < nextOnes.length; i++) {
		if (nextOnes[i].angle !== null) {
			return distance;
		}
		else {
			distance += nextOnes[i]['length'];
		}
	}
	return distance;
};

method.getDistanceToNextStraight = function() {
	if (this.getTrackPiece()['angle'] === null) { // we're on a straight now
		return 0;
	}
	var distance = this.getRemainingDistanceInPiece(); // start with current piece
	var numNextPiecesToCheck = 10;
	var nextOnes = this.track.getNextXPieces(this.getPieceIndex(),numNextPiecesToCheck);
	for (var i = 0; i < nextOnes.length; i++) {
		if (nextOnes[i].angle === null) {
			return distance;
		}
		else {
			distance += nextOnes[i]['length'];
		}
	}
	return distance;
};

method.getNextStraigthLength = function() {
	if (this.getTrackPiece()['angle'] === null) { // we're on a straight now
		return this.getDistanceToNextCorner();
	}
	var distance = 0;
	var numNextPiecesToCheck = 10;
	var nextOnes = this.track.getNextXPieces(this.getPieceIndex(),numNextPiecesToCheck);
	for (var i = 0; i < nextOnes.length; i++) {
		if (nextOnes[i].angle === null) { // straigth piece
			distance = nextOnes[i]['length'];
			for (var j=1; j<nextOnes.length-i; j++) {
				if (nextOnes[(i+j)].angle === null) {
					distance += nextOnes[i+1]['length'];
				} else {
					return distance;
				}
			}
			return distance;
		}
	}
	return distance;
}

method.getNextTurnAngle = function() {
	var numNextPiecesToCheck = 10;
	var nextOnes = this.track.getNextXPieces(this.getPieceIndex(),numNextPiecesToCheck);
	if (this.getTrackPiece()['angle'] === null) { // we're on a straight now
		for (var i = 0; i < nextOnes.length; i++) {
			if (nextOnes[i].getAngle() !== null) {
				return nextOnes[i].getAngle();
			}
		}
		return null; // all straight pieces

	} else {
		return this.getTrackPiece().getAngle();
	}
};

method.getNextTurnRadius = function() {
	var numNextPiecesToCheck = 10;
	var nextOnes = this.track.getNextXPieces(this.getPieceIndex(),numNextPiecesToCheck);
	if (this.getTrackPiece()['radius'] === null) { // we're on a straight now
		for (var i = 0; i < nextOnes.length; i++) {
			if (nextOnes[i].getRadius() !== null) {
				return nextOnes[i].getRadius();
			}
		}
		return null; // all straight pieces

	} else {
		return this.getTrackPiece().getRadius();
	}
};

/**

returns the distance to the other position, ignoring the laps
will return a negative value if otherPos is behind current position, on same piece

**/
method.getDistanceToIgnoreLaps = function(otherPos) {
	// same piece, easy, lemon squeezy
	if (this.pieceIndex === otherPos.pieceIndex) {
		return (otherPos.inPieceDistance - this.inPieceDistance);
	}
	// different piece, start with other's distance from start of piece
	var distance = otherPos.inPieceDistance;
	var curPieceIndex = otherPos.pieceIndex;
	// add distance from start of otherPos's piece to start of my piece
	while (curPieceIndex !== this.pieceIndex) {
		curPieceIndex--;
		if (curPieceIndex < 0) {
			curPieceIndex = this.track.getNumberOfPieces() - 1;
		}
		var curPiece = this.track.getTrackPiece(curPieceIndex);
		if (curPiece.getAngle() === null) {
			distance += curPiece.length;
		}
		else {
			var offset = this.track.getLane(laneIndex).distanceFromCenter;
			distance += curPiece.calculateTurnDistance(offset);
		}
	}
	// remove distance I already travelled in my piece
	distance = distance - this.inPieceDistance;
};