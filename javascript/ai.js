module.exports = AI;
var method = AI.prototype;

function AI(track, cars, myCarJSON) {
	this.track = track;
	this.cars = cars;
	this.throttle = {msgType: "throttle", data: 0.5};
	this.switch_ = null;
	this.canSendSwitch = true;
	this.switchIsSend = false;
	//this.myCar;
	var myCarColor = myCarJSON['color'];
	for (var i=0,n=cars.length; i<n; i++) {
		if (myCarColor == cars[i].getColor()) {
			this.myCar = cars[i];
		}
	}
	this.setThrottle = function(throttle) {
		this.throttle = {msgType: "throttle", data: throttle};
	};
	this.setSwitchLane = function(direction) {

		this.switch_ = {msgType: "switchLane", data: direction};
	};
	this.engageTurbo = function() {
		console.log("TURBO ENGAGED");
		// update carstate!
		this.myCar.getLastCarState().setTurbo(null);
		this.throttle = {msgType: "turbo", data: "go go go"};
	}
}

method.getSwitchToShortestLane = function(gametick) {
	if (!gametick || gametick < 5) {
		return null;
	}
	var myCarPosition = this.myCar.getLastCarState().getPosition();
	var lanes = this.track['lanes'];
	// 1. switch to inside tracklane for shorter track
	// calculate shortest lane between 2 switches
	
	var currentLength = myCarPosition.calculateLaneLengthbetweenNext2Switches(myCarPosition.getEndLaneIndex());
	var bestLaneIndex = null;
	var laneLength = Infinity;
	for (var i=0; i<lanes.length; i++) {
		var length_ = myCarPosition.calculateLaneLengthbetweenNext2Switches(i);
		if (length_ < laneLength) {
			laneLength = length_;
			bestLaneIndex = i;
		}
	}
	if (bestLaneIndex !== myCarPosition['endLaneIndex']) {
		// switch
		if (bestLaneIndex > myCarPosition['endLaneIndex']) {
			this.setSwitchLane('Right');
		} else {
			this.setSwitchLane('Left');
		}
		return this.switch_;
	} 
	return null;
}

/**
 * Only send switchStatement just before switch!
 */
method.getSwitchDecision = function(gametick) {
	if (!gametick || gametick < 5) {
		return null;
	}
	var myCarPosition = this.myCar.getLastCarState().getPosition();
	var nextTrackPiece = this.track.getTrackPiece((myCarPosition.getPieceIndex()+1)%this.track['trackPieces'].length);
	if (nextTrackPiece['switch'] && !this.switchIsSend) {
		this.canSendSwitch = true;
	}
	if (this.track.getTrackPiece(myCarPosition.getPieceIndex())['switch']) {
		this.switchIsSend = false;
	}
	if (!this.canSendSwitch || this.switchIsSend) {
		return null;
	}
	
	var myCarPosition = this.myCar.getLastCarState().getPosition();
	var lanes = this.track['lanes'];
	var responseJSON = null;

	// 1. switch to inside tracklane for shorter track
	// calculate shortest lane between 2 switches
	responseJSON = this.getSwitchToShortestLane(gametick);
	

	// 2. switch when car in front is in same lane,
	// preferably inside, otherwise outside
	// TODO
	// responseJSON = this.getSwitchToFreeLane(gametick);
	this.switchIsSend = true;
	this.canSendSwitch = false;


	// TODO
	return null; //responseJSON
}

method.getSpeedDecision = function(gameTick) {
	var track = this.track;
	var cars = this.cars;
	var myCar = this.myCar;
	var myCarState = myCar.getLastCarState();
	var myCarPosition = myCarState.getPosition();
	var myCarPI = myCarPosition.getPieceIndex();
	var myCarIPD = myCarPosition.getInPieceDistance();
	var isSwitch = track.getTrackPiece(myCarPI)['switch'];
	
	var nextPieces = track.getNextXPieces(myCarPI,5);
	var distanceToNextCorner = myCarPosition.getDistanceToNextCorner();
	var distanceToNextStraight = myCarPosition.getDistanceToNextStraight();
	var speed = myCarState.getAverageSpeed();
	var acceleration = myCarState.getAverageAcceleration();
	var hasTurbo = myCarState.isTurboAvailable();
	var slipAngle = myCarState.getAngle();
	var angleSpeed = myCarState.getAverageAngleSpeed();
	var angleAcceleration = myCarState.getAverageAngleAcceleration();
	var mass = myCar.getMass();
	var cRadius = myCarPosition.getTrackPiece().getRadius();
	var nAngle = myCarPosition.getNextTurnAngle();
	var nRadius = myCarPosition.getNextTurnRadius();
	var inCorner = myCarPosition.getTrackPiece()['angle'] !== null;


	/**
	 * You may switch either to Left or Right. 
	 * The lane switch will occur at the first possible switch 
	 * after message has been received. If multiple commands are sent, 
	 * only the latest will apply. 
	 * Only a single switch can per car can occur 
	 * on the same track piece at a time, 
	 * regardless of the number of lanes on the track.
	 */
//	s('Left'); // why doesn't this work??

	//myCarPosition.getDistanceToNextCorner() * f1 + myCarState.getAverageSpeed() * f2 + nextCorner.alpha / nextCorner.R * f3
	this.setThrottle(0);
	if (inCorner) {
		// TURBO: uit de bocht
		// TODO: berekenen wat het langste rechte stuk is. Nu: is volgend stuk lang genoeg?
		if (hasTurbo) {
			// wanneer er net turbo gegeven wordt vlak voor de finishlijn: alles er uit persen!!!
			if (myCarPosition.getDistanceToNextStraight() < cRadius / 4 && 
				((Math.abs(slipAngle)<40 && Math.abs(angleSpeed)<4) || (Math.abs(slipAngle)<50 && Math.abs(angleSpeed)<1)) &&
				myCarPosition.nextStraightIsToFinish()) 
			{
				console.log("TO FINISH!");
				this.engageTurbo();
				return this.throttle;
			}
			if (myCarPosition.getDistanceToNextStraight() < cRadius / 4 && 
				((Math.abs(slipAngle)<40 && Math.abs(angleSpeed)<4) || (Math.abs(slipAngle)<50 && Math.abs(angleSpeed)<1)) &&
				myCarPosition.getNextStraigthLength() > myCarState.turboTime * myCarState.getAverageSpeed() * myCarState.turboFactor * 1.1 ) 
			{
				this.engageTurbo();
				return this.throttle;
			}
		} 
		
		if (Math.abs(myCarPosition.getTrackPiece()['angle']) < 45) 
		{
			this.setThrottle(1);
		}
		else if ((myCarPosition.getDistanceToNextStraight() < cRadius) && 
			Math.abs(slipAngle) < 40 && 
			Math.abs(angleSpeed) < 2) 
		{
			this.setThrottle(1);
		}
		else if (myCarState.getAverageSpeed() < 7 && 
			Math.abs(myCarState.getAngle()) < 50 && 
			Math.abs(myCarState.getAverageAngleSpeed()) < 2) 
		{
			this.setThrottle(1);
		}
	}
	else {
		if (hasTurbo) {
			if (myCarPosition.getNextStraigthLength() > myCarState.turboTime * myCarState.getAverageSpeed() * myCarState.turboFactor * 1.1 ) 
			{
				this.engageTurbo();
				return this.throttle;
			}
			if (myCarPosition.nextStraightIsToFinish())
			{
				console.log("TO FINISH!");
				this.engageTurbo();
				return this.throttle;
			}
		}
		if (myCarState.getSpeedAtNextTurn() < 0.078 * nRadius) 
		{
			this.setThrottle(1);
		}
		if (myCarPosition.nextStraightIsToFinish()) {
			this.setThrottle(1);
		}
		// if (speed / myCarPosition.getDistanceToNextCorner() < 0.2) {
		// 	this.setThrottle(1);
		// }
	}

	console.log(
		(inCorner ? 'C' : 'S') +myCarState.getPosition().getPieceIndex()+(isSwitch ? '_S' : '')+'\t'+
		this.throttle['data'] +'\t'+
		speed + '\t' +
		// acceleration + '\t' +
		myCarState.getAngle()  + '\t' +
		//this.getDistanceToCarInFront()
		// myCarPosition.calculateLaneLengthbetweenNext2Switches(0)+'\t'+
		// myCarPosition.calculateLaneLengthbetweenNext2Switches(1)
		// myCarState.getAverageAngleSpeed()+'\t'+
		// myCarState.getSpeedAtNextTurn() + '\t' +
		// 0.078 * nRadius + '\t' +
		// (myCarPosition.getNextStraigthLength()+nRadius)+ '\t' +
		// (myCarState.turboTime * myCarState.getAverageSpeed() * myCarState.turboFactor) + '\t' +
		//(Math.pow(Math.pow(speed, 2) + 2 * aApproximation * d, 0.5)) + '\t' +
		//myCarState.getAverageAngleAcceleration()+'\t'+
		//speed * (myCarState.getAngle() * myCarState.getAverageAngleSpeed()) + '\t' +
		//(speed / myCarPosition.getDistanceToNextCorner())+'\t'+
		
	);

    return this.throttle;
};

method.getDistanceToCarInFront = function() {
	var myCar = this.myCar;
	var cars = this.cars;
	var myCarColor = myCar.getColor();
	var myPosition = myCar.getLastCarState().getPosition();
	var myLane = myPosition.getEndLaneIndex(); // end lane index correct? or should this be startLaneIndex?
	var leastDistance = Infinity;
	for (var i = 0; i < cars.length; i++) {
		var car = cars[i];
		var carPosition = car.getLastCarState().getPosition();
		if (car.getColor() !== myCarColor && carPosition.getEndLaneIndex() === myLane) {
			// CAUTION: getDistanceToIgnoreLaps can return negative numbers, very hacky stuff here :)
			var distance = myPosition.getDistanceToIgnoreLaps(carPosition);
			if (distance > 0) {
				leastDistance = Math.min(distance, leastDistance);
			}
		}
	}
	return leastDistance;
};