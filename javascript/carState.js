var Position = require('./position.js');
var physics = require('./physics.js'); // functions only

module.exports = CarState;
var method = CarState.prototype;

/**
 * CarState Class
 *
 * Describes the state of a car at a particular gameTick
 *
 * example of singleCarPositionJSON:
 * {
    "id": {
      "name": "Schumacher",
      "color": "red"
    },
    "angle": 0.0,
    "piecePosition": {
      "pieceIndex": 0,
      "inPieceDistance": 0.0,
      "lane": {
        "startLaneIndex": 0,
        "endLaneIndex": 0
      },
      "lap": 0
    }
  }
 */
function CarState(singleCarPositionJSON, track, gameTick, carRadius, previousCarState){

	this.carRadius = carRadius;
	this.gameTick = gameTick;
	this.angle = singleCarPositionJSON['angle'];
	if (previousCarState != null) {
		this.position = new Position(singleCarPositionJSON['piecePosition'], track, previousCarState.getPosition());
	} else {
		this.position = new Position(singleCarPositionJSON['piecePosition'], track, null);
	}
	
	this.throttle = null; // throttle is calculated by AI based on current carstate.

	// you can only calculate the AVERAGE parameters in the timespan [gameTick - (gameTick-1)] 
	this.averageSpeed = 0; // positive only
	this.averageAcceleration = 0; // positive or negative
	this.averageAngleSpeed = 0; // can be positive or negative!
	this.averageAngleAcceleration = 0; // positive or negative

	// TURBO
	this.turboJSON = null;
	this.turboTime = 0;
	this.turboFactor = 1;

	// console.log(gameTick+'\t'+this.position.getDistanceTravelled()); // carPositionsJSON['gameTick']
	if (previousCarState != null) {
		this.updateCarStatePhysics(previousCarState);
		if (previousCarState.isTurboAvailable()) {
			this.setTurbo(previousCarState.getTurboJSON());
		}
	}
}

/**
 * Update this carState based on previous carState
 */
method.updateCarStatePhysics = function(previousCarState) {
	if (previousCarState) {
		this.averageSpeed = physics.calculateAverageSpeed(this);
		this.averageAcceleration = physics.calculateAverageAcceleration(previousCarState, this);
		this.averageAngleSpeed = physics.calculateAverageAngleSpeed(previousCarState, this);
		this.averageAngleAcceleration = physics.calculateAverageAngleAcceleration(previousCarState, this);
	}
};

method.setThrottle = function(throttle) {
	this.throttle = throttle;
};

method.setTurbo = function(turboJSON) {
	if (turboJSON !== null) {
		this.turboJSON = turboJSON;
		this.turboTime = turboJSON['data'].turboDurationTicks;
		this.turboFactor = turboJSON['data'].turboFactor;
	} else {
		this.turboJSON = null;
	}
	
}

method.isTurboAvailable = function() {return (this.turboJSON !== null);}
method.getTurboJSON = function() {return this.turboJSON;}
method.getThrottle = function() {return this.throttle;}
method.getAngle = function() {return this.angle;};
method.getPosition = function() {return this.position;};
method.getDistanceTravelled = function() {return this.position.getDistanceSinceLastPosition();};
method.getAverageSpeed = function() {return this.averageSpeed;};
method.getAverageAcceleration = function() {return this.averageAcceleration;};
method.getAverageAngleSpeed = function() {return this.averageAngleSpeed;};
method.getAverageAngleAcceleration = function() {return this.averageAngleAcceleration;};

/**
 * WHEN THROTTLE = 1
 * @return {[type]} [description]
 */
method.getSpeedAtNextTurn = function() {
	var d = this.getPosition().getDistanceToNextCorner();
	var mu = 0.10;
	var m = 5;
	var speed = this.getAverageSpeed();
	return (-d * mu / m + Math.pow(Math.pow(d * mu / m, 2) + Math.pow(speed, 2),0.5));
};
