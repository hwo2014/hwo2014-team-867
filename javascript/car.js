var CarState = require('./carState.js');

// http://stackoverflow.com/questions/18188083/javascript-oop-in-nodejs-how
module.exports = Car;
var method = Car.prototype;

/**
 * Car class
 */
function Car(carJSON) {
	// set initial values
	this.lengthValue = carJSON['dimensions']['length'];
	this.widthValue = carJSON['dimensions']['width'];
	this.throttle = 1;
	this.mass = 1.0; // mass is unknown at start
	this.isMassSet = false;
	this.mu = 0.1;
	this.isMuSet = false;
	this.name = carJSON['id']['name'];
	this.color = carJSON['id']['color'];
	this.guideFlagPosition = carJSON['dimensions']['guideFlagPosition'];
	this.carRadius = this.lengthValue / 2 - this.guideFlagPosition; // distance between gravity point and guideFlag

	// TURBO
	this.turboData = null;

	this.carStates = [];
};

method.insertAndUpdateCarState = function(singleCarPositionJSON, track, gameTick) {
	// 1. insert new state
	var carState = null;
	if(this.carStates.length > 0) {
		carState = new CarState(singleCarPositionJSON, track, gameTick, this.carRadius, this.carStates[this.carStates.length-1]);
		// carState.updateCarStatePhysics(this.carStates[this.carStates.length-1]); // update with previous carstate..
	} else { // first carState
		carState = new CarState(singleCarPositionJSON, track, gameTick, this.carRadius, null);
	}

	// PUSH TURBO IN CARSTATE
	if (this.turboData !== null) {
		carState.setTurbo(this.turboData);
		this.turboData = null;
	}

	this.carStates.push(carState); // .. then add to list

	// calculate mass if previous speed = 0 and acceleration > 0 (throttle = 1)
	if (!this.isMassSet &&
		this.carStates.length >=2 &&
		this.carStates[this.carStates.length -2].getAverageSpeed() === 0 && 
		this.carStates[this.carStates.length -1].getAverageAcceleration() > 0 &&
		gameTick < 10) {
		var throttle = 1;
		this.mass = throttle/(this.carStates[this.carStates.length -1].getAverageAcceleration());
		this.isMassSet = true;
	}

	var startat = 10;
	// calculate mu if speed is acceptable high (throttle = 1)
	if (!this.isMuSet &&
		this.carStates.length >= startat &&
		this.carStates[this.carStates.length - startat].getAverageSpeed() > 0) {
		var throttle = this.carStates[this.carStates.length - 2].getThrottle();
		var mu1 = (throttle - this.mass*this.carStates[this.carStates.length-1].getAverageAcceleration()) / this.carStates[this.carStates.length-1].getAverageSpeed();
		if (mu1 > 0) {
			this.mu = (this.mu*(this.carStates.length-startat) + mu1) / (this.carStates.length-startat + 1)
		}
		if(this.carStates.length >= startat + 50) {
			this.isMuSet = true;
		}
	}
}

method.setThrottle = function(throttle) {
	this.throttle = throttle;
}

method.setTurbo = function(turboJSON) {
	this.turboData = turboJSON; 
}

method.getMass = function(){
	return this.mass;
}
method.setMass = function(mass) {
	this.mass = mass;
}

method.getCarState = function(index) {
	return this.carStates[index];
}

method.getLastCarState = function() {
	return this.carStates[this.carStates.length-1]
}

method.getColor = function() {
	return this.color;
}

