/**
 * kinematic equations in 1D:
 *
 * d = v0*t + (a*t^2)/2
 * v = v0 + a*t
 * v^2 = v0^2 + 2*a*d
 *
 * t = gametick 
 * 
 * T = Throttle
 * Tu = Throttle parallel to the track
 * alpha = angle with the track
 * cos(alpha) = Tu/T
 * 
 *
 * => TODO: check if physics behaves like this equation!
 */

// given positions and throttle, calculate average speed between 2 states:
exports.calculateAverageSpeed = function(newCarState) {
	return newCarState.getPosition().getDistanceSinceLastPosition();
};

exports.calculateAverageAcceleration = function(oldCarState, newCarState) {
	return newCarState.getAverageSpeed() - oldCarState.getAverageSpeed();
};

// angle speed of the angle around the guideFlagPosition
exports.calculateAverageAngleSpeed = function(oldCarState, newCarState) {
	var radius = oldCarState.carRadius;
	var angle_difference = newCarState.angle - oldCarState.angle;
	return angle_difference;
};

// angle acceleration of the angle around the guideFlagPosition
exports.calculateAverageAngleAcceleration = function(oldCarState, newCarState) {
	return newCarState.getAverageAngleSpeed() - oldCarState.getAverageAngleSpeed();
};

// can be negative.
calculateArcLength = function(angle, radius) {
	return angle * Math.PI * radius / 180;
};