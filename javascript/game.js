var Car = require('./car.js');
var AI = require('./ai.js');

/**
 * Main (Game) Class
 *
 * // http://nodejs.org/docs/latest/api/modules.html
 */
module.exports = function() {
	var track = null;
	var cars = [];
	var myCar = null;
	var ai = null;
	var turbo = null;

	var getMyCar = function() {
		if (cars.length > 0) {
			for (var j=0, m=cars.length; j < m;j++) {
				if (cars[j].getColor() == myCar['color']) {
					return cars[j];
				}
			}
		}
		return null;
	}

	return {

		/**
		 * {"msgType": "yourCar", "data": {
			  "name": "Schumacher",
			  "color": "red"
			}}
		 */
		setCar: function(yourCarJSON) {
			myCar = yourCarJSON['data'];
		},

		loadData: function (gameInitJSON) {

			// TRACK PIECES
			// ************
			this.trackId = gameInitJSON['data']['race']['track']['id'];
			this.trackName = gameInitJSON['data']['race']['track']['name'];
			var pieces = gameInitJSON['data']['race']['track']['pieces'];
			var lanes = gameInitJSON['data']['race']['track']['lanes'];
			var laps = gameInitJSON['data']['race']['raceSession']['laps'];
			var trackPieces = [];
			for (var i = 0, n = pieces.length; i < n; i++) {
				trackPieces.push(new Piece(pieces[i]));
			}
			track = new Track(trackPieces, lanes, laps);

			// DEBUG LOG
			for (var i = 0, n = trackPieces.length; i < n; i++) {
				var switch_ = "";
				if (trackPieces[i]['switch']) {switch_ = 'switch';}
				console.log('Piece '+i+'. L='+trackPieces[i]['length']+' alpha='+trackPieces[i]['angle']+' R='+trackPieces[i]['radius']+ ' '+switch_);
			}
			
			// CARS
			// ****
			var carsData = gameInitJSON['data']['race']['cars'];
			for (var i = 0, n = carsData.length; i < n; i++) {
				cars.push(new Car(carsData[i]));
			}

			// DEBUG LOG
			// for (var i = 0, n = cars.length; i < n; i++) {
			// 	console.log('car: '+cars[i].name);
			// }

			// A.I.
			// ****
			ai = new AI(track, cars, myCar);

			// TODO: add other information: all cars, ..
			
		},

		getAISwitchDecision: function(carPositionsJSON) {
			var response = ai.getSwitchDecision(carPositionsJSON['gameTick']);
			return response;
		},

		getAISpeedDecision: function(carPositionsJSON) {
			// 1. update carStates of all cars on track (based on color id)
			// ************************************************************
			// The server sends a 'carPositions' between 'Join' and 'gameStart'. This 'carPositions' data has no gametick.
			// The second carPositionsJSON (first one after 'gameStart') has gameTick 1
			if (!carPositionsJSON['gameTick']) {
				carPositionsJSON['gameTick'] = 0;
			}

			// update only your own car.
			for (var i=0, n=carPositionsJSON['data'].length; i < n ; i++) {
				var singleCarPositionJSON = carPositionsJSON['data'][i];
				for (var j=0, m=cars.length; j < m;j++) {
					if (carPositionsJSON['data'][i]['id']['color'] == myCar['color']) {
						cars[j].insertAndUpdateCarState(singleCarPositionJSON, track, carPositionsJSON['gameTick']);
					}
				}
			}

			// 2. get AI decision based on all carStates of all cars + upcoming tracks
			// ***********************************************************************
			var response = ai.getSpeedDecision(carPositionsJSON['gameTick']);

			// update throttle in carstate
			if (response.msgType === 'throttle') {
				for (var i=0, n=carPositionsJSON['data'].length; i < n ; i++) {
					var singleCarPositionJSON = carPositionsJSON['data'][i];
					for (var j=0, m=cars.length; j < m;j++) {
						if (carPositionsJSON['data'][i]['id']['color'] == myCar['color']) {
							cars[j].getLastCarState().setThrottle(response['data']);
						}
					}
				}
			}

			// return response;
			return response;
		},

		setTurbo: function(turboJSON) {
			if(getMyCar() != null) {
				getMyCar().setTurbo(turboJSON);
			}
		}
	};
};

/**
 * (track)Piece Class
 */
function Piece(pieceJSON) {
	this['switch'] = (pieceJSON['switch'] ? true : false);
	if (pieceJSON['length']) {
		this['radius'] = null;
		this['angle'] = null;
		this['length'] = pieceJSON['length'];
	}
	else if (pieceJSON['radius'] && pieceJSON['angle']) {
		this['radius'] = pieceJSON['radius'];
		this['angle'] = pieceJSON['angle'];
		this['length'] = this.calculateTurnDistance(0);
	}
	else {
		if (debug) {
			debug.log('Error: could not calculate length of track piece.');
		}
	}
};
/**
 * Calculate track distance of turn
 * @param   angle: (negative when turn is LEFT)
 * @param   radius
 * @param   centeroffset (can be negative or positive)
 * (negative if left from center? -> TODO: check)
 * @return  distance of track turn
 */
Piece.prototype.calculateTurnDistance = function(centeroffset) {
	var angle = this.angle;
	var radius = this.radius;
 	if (angle < 0) {
 		return Math.abs((radius + centeroffset) * Math.PI * angle/180 );
 	} else {
 		return Math.abs((radius - centeroffset) * Math.PI * angle/180 );
 	}
};
Piece.prototype.getRadius = function() {return this.radius;};
Piece.prototype.getAngle = function() {return this.angle;};
Piece.prototype.getAverageLength = function() {return this['length'];};

/**
 * Complete Track Class
 */
function Track(trackPieces, lanes, laps) {
	this.trackPieces = trackPieces;
	this.lanes = lanes;
	this.totalLaps = laps;
}
Track.prototype.getTotalLaps = function() {return this.totalLaps;}
Track.prototype.getTrackPiece = function(index) {return this.trackPieces[index];};
Track.prototype.getNumberOfPieces = function() {return this.trackPieces.length;};
Track.prototype.getLane = function(index) {return this.lanes[index];};
Track.prototype.getNextXPieces = function(currentPieceIndex, X) {
	var currentPiece = this.getTrackPiece(currentPieceIndex);
	var pieceList = [];
	for (var i=1;i < X; i++) {
		pieceList.push(this.getTrackPiece((currentPieceIndex + i) % this.getNumberOfPieces()));
	}
	return pieceList;
};

